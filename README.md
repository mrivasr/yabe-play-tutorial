## YABE: Yet Another Blog Engine

Play! Framework (1.4.x) example program.
<https://www.playframework.com/documentation/1.4.x/home>

-

We chose to create yet another blog engine. It’s not a very imaginative choice but it will allow us to explore most of the functionality needed by a modern web application.

To make things a bit more interesting, we will manage several users with different roles (editor, admin).

### Usage:
- Eclipsify project running `play eclipsify yabe`
- Import project into eclipse
- Use yabe.launch from eclipse folder or just `play run`