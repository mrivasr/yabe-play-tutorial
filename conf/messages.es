title=Título
content=Contenido
postedAt=Posteado a
author=Autor
post=Post relacionado
tags=Set de tags
name=Primer nombre
email=Email
password=Password
fullName=Nombre completo
isAdmin=Usuario es admin

secure.username=Tu email:
secure.password=Tu password:
secure.signin=Log in ahora

views.main.tools.login=Log in para escribir algo
views.main.tools.about=Acerca de este blog
views.main.footer = Yabe es un potente (no mucho) motor de blog construido con <a href="https://www.playframework.com">Play framework</a> como una aplicación tutorial.



post = artículo 
Post = Artículo 
posts = artículos 
Posts = Artículos 
comment = comentario 
Comment = Comentario 
comments = comentarios 
Comments = Comentarios 
user = usuario 
User = Usuario 
users = usuarios 
Users = Usuarios
tag= tag
Tag = Tag
tags = tags
Tags = Tags

views.Application.listTaggedMultiple.title = Hay %1$s artículos tageados con %2$s
views.Application.listTagged.title = Hay 1 artículo tageado con %1$s
views.Application.notag.title = No hay artículos tageados con %1$s
views.Application.index.olderpost = Artículos antiguos <span class="from">de este blog
views.Application.index.nothingtosee = No hay nada para leer aquí.

views.Application.show.name = Tu nombre: 
views.Application.show.message = Tu mensaje: 
views.Application.show.code = Por favor, introduzca el código de abajo: 
views.Application.show.submit= "Publicar comentario"


